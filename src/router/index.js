import Vue from 'vue';
import Router from 'vue-router';

import routes from './routes';
import { checkAuthentication, checkAuthorization } from './guards';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes,
});

router.beforeEach(checkAuthentication);
router.beforeEach(checkAuthorization);

export default router;
