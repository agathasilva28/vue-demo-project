import * as guards from './guards';
import * as token from '@/services/auth/token';
import * as permissions from '@/services/auth/permission';

describe('routes/guards', () => {
  let to;
  let next;

  beforeEach(() => {
    next = jest.fn();
  });

  describe('checkAuthentication', () => {
    describe('when route is login', () => {
      beforeEach(() => {
        to = { name: 'Login' };
        guards.checkAuthentication(to, {}, next);
      });

      it('calls next to continue', () => {
        expect(next).toBeCalledWith();
      });
    });

    describe('when route is not login', () => {
      beforeEach(() => {
        to = { name: 'Product', fullPath: '/products' };
      });

      describe('and token not exists', () => {
        beforeEach(() => {
          token.exists = jest.fn(() => false);
          guards.checkAuthentication(to, {}, next);
        });

        it('calls next with login path and with redirect query to current route', () => {
          expect(next).toBeCalledWith({
            path: '/login',
            query: { redirect: '/products' },
          });
        });
      });

      describe('and token exists', () => {
        beforeEach(() => {
          token.exists = jest.fn(() => true);
          guards.checkAuthentication(to, {}, next);
        });

        it('calls next to continue', () => {
          expect(next).toBeCalledWith();
        });
      });
    });
  });

  describe('checkAuthorization', () => {
    describe('when agent is admin', () => {
      beforeEach(() => {
        permissions.isAdmin = jest.fn(() => true);
        guards.checkAuthorization(to, {}, next);
      });

      it('calls next to continue', () => {
        expect(next).toBeCalledWith();
      });
    });

    describe('when agent is not admin', () => {
      beforeEach(() => {
        permissions.isAdmin = jest.fn(() => false);
        to = {
          matched: [
            { meta: { authorize: 'ADMIN_ROLE' } },
          ],
        };
      });

      describe('and agent has at least one of the allowed roles', () => {
        beforeEach(() => {
          permissions.hasAnyRole = jest.fn(() => true);
          guards.checkAuthorization(to, {}, next);
        });

        it('calls next to continue', () => {
          expect(next).toBeCalledWith();
        });

        it('calls permissions has any role with routes roles', () => {
          expect(permissions.hasAnyRole).toBeCalledWith(['ADMIN_ROLE']);
        });
      });

      describe('and agent not has any of the allowed roles', () => {
        beforeEach(() => {
          permissions.hasAnyRole = jest.fn(() => false);
          guards.checkAuthorization(to, {}, next);
        });

        it('calls next with not found page', () => {
          expect(next).toBeCalledWith({ path: '/not-found' });
        });

        it('calls permissions has any role with routes roles', () => {
          expect(permissions.hasAnyRole).toBeCalledWith(['ADMIN_ROLE']);
        });
      });
    });
  });
});
