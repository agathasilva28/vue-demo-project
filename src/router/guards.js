import Vue from 'vue';
import * as token from '@/services/auth/token';
import * as permissions from '@/services/auth/permission';
import roles from '@/services/config/roles';

export const checkAuthentication = (to, from, next) => {
  const whitelist = ['Login', 'Signup'];

  if (whitelist.includes(to.name)) {
    return next();
  }

  if (!token.exists()) {
    return next({
      path: '/login',
      query: { redirect: to.fullPath },
    });
  }

  return next();
};

export const checkAuthorization = (to, from, next) => {
  Vue.prototype.$userHasRoles = permissions.hasAnyRole;
  Vue.prototype.$roles = roles;

  if (permissions.isAdmin()) {
    return next();
  }

  const allowedRoles = to.matched.reduce(
    (authorizations, route) => {
      if (!route.meta.authorize) {
        return authorizations;
      }
      
      return [...authorizations, route.meta.authorize];
    }, [],
  );

  if (!allowedRoles.length || permissions.hasAnyRole(allowedRoles)) {
    return next();
  }

  return next({ path: '/not-found' });
};
