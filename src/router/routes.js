import Signup from '@/screens/Signup/components/Main';
import Login from '@/screens/Login/components/Main';
import Index from '@/screens/Index/components/Main';
import NotFound from '@/screens/NotFound/components/Main';
import User from '@/screens/Admin/User/components/Main';
import UserNew from '@/screens/Admin/User/components/New';
import UserEdit from '@/screens/Admin/User/components/Edit';
import UserIndex from '@/screens/Admin/User/components/Index';
import AdminMeal from '@/screens/Admin/Meal/components/Main';
import AdminMealIndex from '@/screens/Admin/Meal/components/Index';
import AdminMealEdit from '@/screens/Admin/Meal/components/Edit';
import AdminMealNew from '@/screens/Admin/Meal/components/New';
import Meal from '@/screens/Meal/components/Main';
import MealNew from '@/screens/Meal/components/New';
import MealEdit from '@/screens/Meal/components/Edit';
import MealIndex from '@/screens/Meal/components/Index';

import roles from '@/services/config/roles';

export default [
  {
    path: '/',
    name: 'Index',
    component: Index,
  },
  {
    name: 'Signup',
    path: '/signup',
    component: Signup,
  },
  {
    name: 'Login',
    path: '/login',
    component: Login,
  },
  {
    name: 'NotFound',
    path: '/not-found',
    component: NotFound,
  },
  {
    path: '/users',
    component: User,
    meta: {
      authorize: roles.MANAGER,
    },
    children: [
      {
        name: 'UserIndex',
        path: '/',
        component: UserIndex,
      },
      {
        name: 'UserNew',
        path: 'new',
        component: UserNew,
      },
      {
        name: 'UserEdit',
        path: ':id/edit',
        component: UserEdit,
      },
    ],
  },
  {
    path: '/admin/meals',
    component: AdminMeal,
    meta: {
      authorize: roles.ADMIN,
    },
    children: [
      {
        name: 'AdminMealIndex',
        path: ':id/',
        component: AdminMealIndex,
      },
      {
        name: 'AdminMealNew',
        path: ':id/new',
        component: AdminMealNew,
      },
      {
        name: 'AdminMealEdit',
        path: 'meals/:id/edit',
        component: AdminMealEdit,
      },
    ],
  },
  {
    path: '/meals',
    component: Meal,
    meta: {
      authorize: roles.REGULAR,
    },
    children: [
      {
        name: 'MealIndex',
        path: '/',
        component: MealIndex,
      },
      {
        name: 'MealNew',
        path: 'new',
        component: MealNew,
      },
      {
        name: 'MealEdit',
        path: ':id/edit',
        component: MealEdit,
      },
    ],
  },
];
