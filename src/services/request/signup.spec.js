import request from './signup';

describe('services/request/signup', () => {
  let data;

  beforeEach(() => {
    data = {
      param: 'test',
    };
  });

  describe('post', () => {
    beforeEach(() => {
      request.post = jest.fn();
    });

    it('Call with Signup path', () => {
      request.post(data);

      expect(request.post).toHaveBeenCalledWith(data);
    });
  });
});
