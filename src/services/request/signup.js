import axios from 'axios';

const api = {};

api.post = (data) => {
  const headers = { 'Content-Type': 'application/json' };
  const url = 'http://calories-counter-api.local/user/signup';

  return axios
    .post(url, data, { headers })
    .then((response) => {
      const { data } = response;
      return data;
    });
};

export default api;
