import axios from 'axios';
import * as token from '@/services/auth/token';
import * as permissions from '@/services/auth/permission';

const api = {};

api.post = (credentials) => {
  const headers = { 'Content-Type': 'application/json' };
  const url = 'http://calories-counter-api.local/user/auth';

  return axios
    .post(url, credentials, { headers })
    .then((response) => {
      const { data } = response;
      token.set(data.token);
      permissions.set(data.role);
      return data;
    });
};

export default api;
