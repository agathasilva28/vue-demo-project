import User from './user';
import * as token from '@/services/auth/token';

describe('services/request/user', () => {
  let path;
  let data;
  let headers;
  let request;
  let resource;

  beforeEach(() => {
    path = '/user/1';
    resource = 1;

    data = {
      param: 'test',
    };

    headers = {
      Authorization: `Token token=${token.get()}`,
      Accept: 'application/json',
    };

    request = new User();
  });

  describe('post', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'post');
    });

    it('Call with User path', () => {
      request.post(resource, data);

      expect(request.axios.post).toHaveBeenCalledWith(path, data, { headers });
    });
  });
});
