import Meal from './meal';
import * as token from '@/services/auth/token';

describe('services/request/meal', () => {
  let path;
  let data;
  let headers;
  let request;
  let resource;

  beforeEach(() => {
    path = '/meals/1';
    resource = 1;

    data = {
      param: 'test',
    };

    headers = {
      Authorization: `Token token=${token.get()}`,
      Accept: 'application/json',
    };

    request = new Meal();
  });

  describe('post', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'post');
    });

    it('Call with Meal path', () => {
      request.post(resource, data);

      expect(request.axios.post).toHaveBeenCalledWith(path, data, { headers });
    });
  });
});
