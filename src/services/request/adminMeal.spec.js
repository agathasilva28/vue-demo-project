import AdminMeal from './adminMeal';
import * as token from '@/services/auth/token';

describe('services/request/adminMeal', () => {
  let path;
  let data;
  let headers;
  let request;
  let resource;

  beforeEach(() => {
    path = '/admin/meals/1';
    resource = 1;

    data = {
      param: 'test',
    };

    headers = {
      Authorization: `Token token=${token.get()}`,
      Accept: 'application/json',
    };

    request = new AdminMeal();
  });

  describe('post', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'post');
    });

    it('Call with Meal path', () => {
      request.post(resource, data);

      expect(request.axios.post).toHaveBeenCalledWith(path, data, { headers });
    });
  });
});
