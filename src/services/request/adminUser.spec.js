import AdminUser from './adminUser';
import * as token from '@/services/auth/token';

describe('services/request/adminUser', () => {
  let path;
  let data;
  let headers;
  let request;
  let resource;

  beforeEach(() => {
    path = '/admin/users/1';
    resource = 1;

    data = {
      param: 'test',
    };

    headers = {
      Authorization: `Token token=${token.get()}`,
      Accept: 'application/json',
    };

    request = new AdminUser();
  });

  describe('post', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'post');
    });

    it('Call with User path', () => {
      request.post(resource, data);

      expect(request.axios.post).toHaveBeenCalledWith(path, data, { headers });
    });
  });
});
