import API from './index';
import * as token from '@/services/auth/token';

describe('services/request/api/index', () => {
  let path;
  let data;
  let request;
  let headers;
  let resource;

  beforeEach(() => {
    path = '/test';

    data = {
      param: 'test',
    };

    resource = 1;

    request = new API({ path });

    headers = {
      Authorization: `Token token=${token.get()}`,
      Accept: 'application/json',
    };
  });

  describe('axios instance', () => {
    it('Has a axios instance', () => {
      expect(request.axios).toBeDefined();
    });
  });

  describe('post', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'post');
    });

    describe('When call with data', () => {
      it('Call with data', () => {
        request.post(resource, data);

        expect(request.axios.post).toHaveBeenCalledWith(`${path}/${resource}`, data, { headers });
      });
    });
  });

  describe('put', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'put');
    });

    beforeEach(() => {
      path = `/test/${resource}`;
    });

    describe('When call with data', () => {
      it('Call with data', () => {
        request.put(resource, data);

        expect(request.axios.put).toHaveBeenCalledWith(path, data, { headers });
      });
    });
  });

  describe('get', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'get');
    });

    beforeEach(() => {
      path = `/test/${resource}`;
    });

    describe('When call with data', () => {
      it('Call with data', () => {
        request.get(resource, data);

        expect(request.axios.put).toHaveBeenCalledWith(path, data, { headers });
      });
    });
  });

  describe('delete', () => {
    beforeEach(() => {
      jest.spyOn(request.axios, 'delete');
    });

    beforeEach(() => {
      path = `/test/${resource}`;
    });

    describe('When call with data', () => {
      it('Call with data', () => {
        request.delete(resource);

        expect(request.axios.put).toHaveBeenCalledWith(path, data, { headers });
      });
    });
  });
});
