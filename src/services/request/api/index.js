import instance from './instance';
import * as token from '@/services/auth/token';

const configHeader = () => ({
  headers: {
    Accept: 'application/json',
    Authorization: `Token token=${token.get()}`,
  },
});

export default class API {
  constructor(config = {}) {
    this.path = config.path;

    this.axios = instance();
  }

  post(resource = {}, data) {
    const path = `${this.path}/${resource}`;
    return this.axios.post(path, data, configHeader());
  }

  put(resource, data) {
    const path = `${this.path}/${resource}`;
    return this.axios.put(path, data, configHeader());
  }

  get(resource = '', config = {}) {
    return this.axios.get(`${this.path}/${resource}`, { ...configHeader(), ...config });
  }

  delete(resource = '') {
    return this.axios.delete(`${this.path}/${resource}`, { ...configHeader() });
  }
}
