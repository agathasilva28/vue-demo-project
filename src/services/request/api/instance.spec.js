import axios from 'axios';
import instance from './instance';

describe('services/request/api/instance', () => {
  let instanceData;
  let interceptor;
  let axiosInstance;

  beforeEach(() => {
    interceptor = jest.fn();
    instanceData = { baseURL: 'http://calories-counter-api.local', interceptors: { response: { use: interceptor } } };
    axios.create = jest.fn(() => (instanceData));
  });

  describe('when is first call', () => {
    beforeEach(() => {
      axiosInstance = instance();
    });

    it('calls axios create with base url', () => {
      expect(axios.create).toBeCalledWith({ baseURL: 'http://calories-counter-api.local' });
    });

    it('returns axios instance', () => {
      expect(axiosInstance.baseURL).toEqual(instanceData.baseURL);
    });
  });

  describe('when is second call', () => {
    beforeAll(() => {
      axiosInstance = instance();
    });

    it('not calls axios create with base url', () => {
      expect(axios.create).not.toBeCalledWith(instanceData);
    });

    it('returns axios instance', () => {
      expect(axiosInstance.baseURL).toEqual(instanceData.baseURL);
    });
  });
});
