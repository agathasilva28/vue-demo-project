import axios from 'axios';

let instance;

export default () => {
  if (instance) {
    return instance;
  }

  instance = axios.create(
    {
      baseURL: "http://calories-counter-api.local",
    },
  );

  instance.interceptors.response.use((response) => response);

  return instance;
};
