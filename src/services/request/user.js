import API from './api';

export default class Users extends API {
  constructor(config = {}) {
    config.path = '/user';

    super(config);
  }
}
