import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import * as token from '@/services/auth/token';
import * as permission from '@/services/auth/permission';
import auth from './auth';

describe('services/request/auth', () => {
  describe('post', () => {
    let mock;
    let credentials;
    let userToken;
    let userRole;

    beforeEach((done) => {
      mock = new MockAdapter(axios);

      credentials = { email: 'email@email.com' };

      userToken = 'hjfg676vk';
      userRole = 'ADMIN_ROLE';

      mock.onPost('http://calories-counter-api.local/user/auth').reply(201, { token: userToken, role: userRole });

      token.set = jest.fn();
      permission.set = jest.fn();

      auth.post(credentials).then(() => done());
    });

    it('set token with data', () => {
      expect(token.set).toBeCalledWith(userToken);
    });

    it('set user roles with roles data', () => {
      expect(permission.set).toBeCalledWith(userRole);
    });
  });
});
