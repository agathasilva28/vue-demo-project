import roles from './roles';

describe('services/config/roles', () => {
  it('should have all admins roles', () => {
    expect(roles.REGULAR).toEqual('REGULAR');
  });

  it('should have SUPER_ADMIN role', () => {
    expect(roles.MANAGER).toEqual('MANAGER');
  });

  it('should have FL_ADMIN role', () => {
    expect(roles.ADMIN).toEqual('ADMIN');
  });
});
