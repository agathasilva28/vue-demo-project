import * as permission from './permission';

describe('services/auth/permission', () => {
  let key;

  beforeEach(() => {
    key = 'calories-counter-auth-agent-roles';
    window.localStorage.removeItem(key);
  });

  describe('set', () => {
    let userRoles;

    beforeEach(() => {
      userRoles = 'ROLE';
      permission.set(userRoles);
    });

    it('calls set item on local storage with roles', () => {
      expect(window.localStorage.getItem(key)).toEqual('ROLE');
    });
  });

  describe('isAdmin', () => {
    describe('when agent has super admin on roles', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, ['ADMIN']);
      });

      it('returns true', () => {
        expect(permission.isAdmin()).toBeTruthy();
      });
    });

    describe('when agent not has super admin on roles', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, ['MANAGER']);
      });

      it('returns false', () => {
        expect(permission.isAdmin()).toBeFalsy();
      });
    });
  });

  describe('hasAnyRole', () => {
    let hasPermission;

    describe('when agent has admin role', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, ['ADMIN']);
        hasPermission = permission.hasAnyRole(['MANAGER', 'ADMIN']);
      });

      it('returns true', () => {
        expect(hasPermission).toBeTruthy();
      });
    });

    describe('when agent has some of provided roles', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, ['MANAGER']);
        hasPermission = permission.hasAnyRole(['MANAGER', 'ADMIN']);
      });

      it('returns true', () => {
        expect(hasPermission).toBeTruthy();
      });
    });

    describe('when agent not has some of provided roles', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, ['REGULAR']);
        hasPermission = permission.hasAnyRole(['MANAGER', 'ADMIN']);
      });

      it('returns false', () => {
        expect(hasPermission).toBeFalsy();
      });
    });
  });
});
