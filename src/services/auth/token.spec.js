import * as token from './token';

describe('services/auth/token', () => {
  let key;

  beforeEach(() => {
    key = 'calories-counter-auth-token';
    window.localStorage.removeItem(key);
  });

  describe('set', () => {
    let agentToken;

    beforeEach(() => {
      agentToken = 'gdf8767fved';
      token.set(agentToken);
    });

    it('calls set item on local storage with token', () => {
      expect(window.localStorage.getItem(key)).toEqual(agentToken);
    });
  });

  describe('get', () => {
    let agentToken;

    beforeEach(() => {
      agentToken = 'gdf8767fved';

      window.localStorage.setItem(key, agentToken);
    });

    it('returns token on local storage', () => {
      expect(token.get()).toEqual(agentToken);
    });
  });

  describe('exists', () => {
    let agentToken;

    beforeEach(() => {
      agentToken = 'gdf8767fved';
    });

    describe('when has token on local storage', () => {
      beforeEach(() => {
        window.localStorage.setItem(key, agentToken);
      });

      it('returns true', () => {
        expect(token.exists()).toBeTruthy();
      });
    });

    describe('when has not token on local storage', () => {
      it('returns false', () => {
        expect(token.exists()).toBeFalsy();
      });
    });
  });

  describe('destroy', () => {
    let agentToken;

    beforeEach(() => {
      agentToken = 'gdf8767fved';
      window.localStorage.setItem(key, agentToken);

      token.destroy();
    });

    it('removes item on local storage with key', () => {
      expect(window.localStorage.getItem(key)).toBeNull();
    });
  });
});
