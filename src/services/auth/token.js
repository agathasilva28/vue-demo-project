const KEY = 'calories-counter-auth-token';

export const set = (token) => {
  window.localStorage.setItem(KEY, token);
};

export const get = () => window.localStorage.getItem(KEY);

export const exists = () => {
  const token = get();
  return !!token && token !== 'undefined';
};

export const destroy = () => {
  window.localStorage.removeItem(KEY);
};
