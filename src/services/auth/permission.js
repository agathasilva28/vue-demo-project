const KEY = 'calories-counter-auth-agent-roles';

export const set = (role) => {
  window.localStorage.setItem(KEY, role);
};

export const currentUserRole = () => {
  const role = window.localStorage.getItem(KEY);

  if (!role) {
    return '';
  }

  return role
};

export const isAdmin = () => currentUserRole() == 'ADMIN';

export const hasAnyRole = (allowedRoles) => {
  if (isAdmin()) {
    return true;
  }
  
  const role = currentUserRole()
  return allowedRoles.includes(role);
};
