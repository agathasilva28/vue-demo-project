import Vue from 'vue';
import Vuex from 'vuex';

import login from '@/screens/Login/store';
import signup from '@/screens/Signup/store';
import adminUser from '@/screens/Admin/User/store';
import adminMeal from '@/screens/Admin/Meal/store';
import meal from '@/screens/Meal/store';
import user from '@/screens/User/store';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    adminUser,
    adminMeal,
    signup,
    login,
    meal,
    user,
  },
});
