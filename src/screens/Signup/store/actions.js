import router from '@/router';
import api from '@/services/request/signup';
import { NotificationProgrammatic as Notification } from 'buefy'

const actions = {};

const notificationErrorData = {
  message: 'something went wrong, try again later!',
  position: 'is-bottom-left',
  type: 'is-danger',
  hasIcon: true
}

actions.create = (_, { data }) => api
  .post(data)
  .then(() => router.push({ path: '/login' }))
  .catch(() => Notification.open(notificationErrorData));

export default actions;
