import router from '@/router';
import api from '@/services/request/auth';
import { NotificationProgrammatic as Notification } from 'buefy'

const actions = {};

const notificationErrorData = {
  message: 'something went wrong, try again later!',
  position: 'is-bottom-left',
  type: 'is-danger',
  hasIcon: true
}

actions.authenticate = (_, { credentials, redirectPath }) => api
  .post(credentials)
  .then(() => router.push({ path: redirectPath }))
  .catch(() => Notification.open(notificationErrorData));

export default actions;
