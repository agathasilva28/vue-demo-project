import MockAdapter from 'axios-mock-adapter';
import axios from 'axios';

import router from '../../../router';
import actions from './actions';

describe('screens/Login/store/actions', () => {
  let mock;

  beforeEach(() => {
    mock = new MockAdapter(axios);

    router.push = jest.fn();
  });

  describe('authenticate', () => {
    let credentials;
    let redirectPath;

    const mockResponse = (status) => {
      mock.onPost('http://calories-counter-api.local/user/auth').reply(status, {});
    };

    const authenticate = (done) => {
      actions.authenticate({}, { credentials, redirectPath }).then(() => done());
    };

    beforeEach(() => {
      credentials = { email: 'admin@admin.com', password: 'admin' };
    });

    describe('when authenticate', () => {
      beforeEach((done) => {
        redirectPath = '/';
        mockResponse(201);
        authenticate(done);
      });

      it('calls post and push to redirect path if response is ok', () => {
        expect(router.push).toBeCalledWith({ path: '/' });
      });
    });
  });
});
