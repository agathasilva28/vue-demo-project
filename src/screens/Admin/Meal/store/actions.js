import Meal from '@/services/request/adminMeal';
import router from '@/router';
import types from './types';
import { NotificationProgrammatic as Notification } from 'buefy'

const actions = {};
const api = new Meal();

const notificationErrorData = {
  message: 'something went wrong, try again later!',
  position: 'is-bottom-right',
  type: 'is-danger',
  hasIcon: true
}

actions.create = ({ state }) => api.post('', state.active)
  .then(() =>  router.push(`/admin/meals/${state.active.user_id}`))
  .catch(() => Notification.open(notificationErrorData));

actions.update = ({ state }) => api.put(state.active.id, state.active)
  .then(() => {
    router.push(`/admin/meals/${state.active.user_id}`);
  })
  .catch(() => Notification.open(notificationErrorData));

actions.delete = ({ commit, state }, id) => api.delete(`${id}`).then(() => {
  actions.getList({ commit, state });
});

actions.getMeal = ({ commit }, id) => api.get(`${id}`).then((response) => {
  commit(types.SET_ACTIVE, response.data);
});

actions.getList = ({ commit, state }) => api.get('', { params: state.filters })
  .then((response) => {
    commit(types.SET_LIST, response.data);
  });

actions.updateActive = ({ commit }, data) => commit(types.UPDATE_ACTIVE, data);

actions.resetActive = ({ commit }) => commit(types.RESET_ACTIVE);

actions.updateFilters = ({ commit }, data) => commit(types.UPDATE_FILTERS, data);

export default actions;
