const types = {};

types.RESET_ACTIVE = 'RESET_ACTIVE';
types.UPDATE_ACTIVE = 'UPDATE_ACTIVE';
types.UPDATE_FILTERS = 'UPDATE_FILTERS';
types.SET_ACTIVE = 'SET_ACTIVE';
types.SET_LIST = 'SET_LIST';

export default types;
