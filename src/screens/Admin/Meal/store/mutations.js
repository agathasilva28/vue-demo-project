import types from './types';

const defaultActive = {
  description: 'New Meal',
  eat_time: new Date(),
};

export const state = {
  list: [],
  active: defaultActive,
  filters: {},
};

export const mutations = {
  [types.SET_ACTIVE](state, data) {
    state.active = data;
  },
  [types.UPDATE_ACTIVE](state, { property, value }) {
    state.active = { ...state.active, [property]: value };
  },
  [types.RESET_ACTIVE](state) {
    state.active = defaultActive;
  },
  [types.UPDATE_FILTERS](state, { property, value }) {
    state.filters = { ...state.filters, [property]: value };
  },
  [types.SET_LIST](state, data) {
    state.list = data;
  },
};
