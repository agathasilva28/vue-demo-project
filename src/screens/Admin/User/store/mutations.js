import types from './types';

const defaultActive = {
  name: 'New User'
};

const defaultFilters = {
  page: '1',
};

export const state = {
  list: [],
  active: defaultActive,
  filters: defaultFilters,
};

export const mutations = {
  [types.SET_ACTIVE](state, data) {
    state.active = data;
  },
  [types.UPDATE_ACTIVE](state, { property, value }) {
    state.active = { ...state.active, [property]: value };
  },
  [types.UPDATE_FILTERS](state, { property, value }) {
    state.filters = { ...state.filters, [property]: value };
  },
  [types.RESET_ACTIVE](state) {
    state.active = defaultActive;
  },
  [types.SET_LIST](state, data) {
    state.list = data;
  },
};
