import { mutations } from './mutations';

describe('screens/Admin/User/store/mutations', () => {
  let state;

  beforeEach(() => {
    state = {
      active: {},
    };
  });

  it('SET_ACTIVE', () => {
    const user = { id: 1, name: 'User' };
    mutations.SET_ACTIVE(state, user);

    expect(state.active).toEqual(user);
  });

  it('RESET_ACTIVE', () => {
    mutations.RESET_ACTIVE(state);

    expect(state.active).toEqual({
      name: 'New User'
    });
  });

  it('UPDATE_ACTIVE', () => {
    const user = { id: 1, name: 'User' };
    state.active = user;
    mutations.UPDATE_ACTIVE(state, { property: 'name', value: 'User 2' });

    expect(state.active).toEqual({ id: 1, name: 'User 2' });
  });

  it('UPDATE_FILTERS', () => {
    const filter = { page: 1 };
    state.active = filter;
    mutations.UPDATE_ACTIVE(state, { property: 'page', value: 2 });

    expect(state.active).toEqual({ page: 2 });
  });

  it('SET_LIST', () => {
    const users = [{ id: 1, name: 'User' }];
    mutations.SET_LIST(state, users);

    expect(state.list).toEqual(users);
  });
});
