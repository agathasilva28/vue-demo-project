import MockAdapter from 'axios-mock-adapter';

import User from '@/services/request/adminUser';
import router from '@/router';

import actions from './actions';
import types from './types';

jest.mock('buefy');

describe('screens/Admin/User/store/actions', () => {
  let api;
  let mock;
  let dispatch;
  let url;

  beforeEach(() => {
    url = 'http://calories-counter-api.local/admin/users/';
    api = new User();
    mock = new MockAdapter(api.axios);

    dispatch = jest.fn();
  });

  describe('create', () => {
    let state;

    beforeEach(() => {
      state = { active: { name: 'User' } };

      router.push = jest.fn();
    });

    describe('when is success', () => {
      let data;

      beforeEach((done) => {
        data = {
          id: 1,
          name: 'User',
        };

        mock.onPost(url).reply(200, data);

        actions.create({ dispatch, state }).then(() => {
          done();
        });
      });

      it('calls router push with Users list', () => {
        expect(router.push).toBeCalledWith('/users');
      });
    });

    describe('when has error', () => {
      beforeEach((done) => {
        mock.onPost(url).reply(401, {});
        actions.create({ dispatch, state }).then(() => done());
      });

      it('not calls router push', () => {
        expect(router.push).not.toBeCalled();
      });
    });
  });

  describe('update', () => {
    let state;

    beforeEach(() => {
      state = { active: { id: 1, name: 'User' } };

      router.push = jest.fn();
    });

    describe('when is success', () => {
      let data;

      beforeEach((done) => {
        data = {
          id: 1,
          name: 'User',
        };

        mock.onPut(`${url}1`).reply(200, data);

        actions.update({ dispatch, state }).then(() => {
          done();
        });
      });

      it('calls router push with Users list', () => {
        expect(router.push).toBeCalledWith('/users');
      });
    });

    describe('when has error', () => {
      beforeEach((done) => {
        mock.onPut(url).reply(401, {});
        actions.update({ dispatch, state }).then(() => done());
      });

      it('not calls router push', () => {
        expect(router.push).not.toBeCalled();
      });
    });
  });

  describe('updateActive', () => {
    let commit;

    beforeEach(() => {
      commit = jest.fn();

      actions.updateActive({ commit }, { id: 1 });
    });

    it('calls commit on update active with data', () => {
      expect(commit).toBeCalledWith(types.UPDATE_ACTIVE, { id: 1 });
    });
  });

  describe('resetActive', () => {
    let commit;

    beforeEach(() => {
      commit = jest.fn();

      actions.resetActive({ commit });
    });

    it('calls commit on reset active', () => {
      expect(commit).toBeCalledWith(types.RESET_ACTIVE);
    });
  });

  describe('getList', () => {
    let commit;
    let response;
    let state;

    beforeEach((done) => {
      response = [{ id: 1 }];
      state = { filters: { is_active: false } };

      mock.onGet('/admin/users/', { params: state.filters }).reply(200, response);

      commit = jest.fn();

      actions.getList({ commit, state }).then(() => done());
    });

    it('calls commit with response data', () => {
      expect(commit).toBeCalledWith(types.SET_LIST, response);
    });
  });

  describe('getUser', () => {
    let commit;
    let response;
    let id;

    beforeEach((done) => {
      response = { id: 1, name: 'User' };
      id = 1;

      mock.onGet('/admin/users/1').reply(200, response);

      commit = jest.fn();

      actions.getUser({ commit }, id).then(() => done());
    });

    it('calls commit with response data', () => {
      expect(commit).toBeCalledWith(types.SET_ACTIVE, response);
    });
  });
});
