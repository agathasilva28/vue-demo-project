import getters from './getters';

describe('screens/Admin/User/store/getters', () => {
  let state;

  beforeEach(() => {
    state = {
      list: [{ id: 1, name: 'User' }],
      active: { id: 1, name: 'User' },
    };
  });
  
  it('list', () => {
    expect(getters.list(state)).toEqual(state.list);
  });

  it('filters', () => {
    expect(getters.filters(state)).toEqual(state.filters);
  });

  it('active', () => {
    expect(getters.active(state)).toEqual(state.active);
  });
});
