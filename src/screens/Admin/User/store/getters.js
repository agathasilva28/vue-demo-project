const getters = {};

getters.active = (state) => state.active;
getters.filters = (state) => state.filters;
getters.list = (state) => state.list;

export default getters;
