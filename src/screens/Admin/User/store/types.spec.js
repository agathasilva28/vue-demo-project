import types from './types';

describe('screens/Admin/store/types', () => {
  it('RESET_ACTIVE', () => {
    expect(types.RESET_ACTIVE).toEqual('RESET_ACTIVE');
  });

  it('UPDATE_ACTIVE', () => {
    expect(types.UPDATE_ACTIVE).toEqual('UPDATE_ACTIVE');
  });

  it('SET_ACTIVE', () => {
    expect(types.SET_ACTIVE).toEqual('SET_ACTIVE');
  });

  it('UPDATE_FILTERS', () => {
    expect(types.UPDATE_FILTERS).toEqual('UPDATE_FILTERS');
  });

  it('SET_LIST', () => {
    expect(types.SET_LIST).toEqual('SET_LIST');
  });
});
