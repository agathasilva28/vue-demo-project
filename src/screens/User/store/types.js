const types = {};

types.RESET_ACTIVE = 'RESET_ACTIVE';
types.UPDATE_ACTIVE = 'UPDATE_ACTIVE';
types.SET_ACTIVE = 'SET_ACTIVE';

export default types;
