import User from '@/services/request/user';
import router from '@/router';
import types from './types';
import { NotificationProgrammatic as Notification } from 'buefy'

const actions = {};
const api = new User();

const notificationErrorData = {
  message: 'something went wrong, try again later!',
  position: 'is-bottom-right',
  type: 'is-danger',
  hasIcon: true
}

actions.update = ({ state }) => api.put(state.active.id, state.active)
  .then(() => {
    router.push('/meals');
  })
  .catch(() => Notification.open(notificationErrorData));

actions.getUser = ({ commit }) => api.get('').then((response) => {
  commit(types.SET_ACTIVE, response.data);
});

actions.updateActive = ({ commit }, data) => commit(types.UPDATE_ACTIVE, data);

actions.resetActive = ({ commit }) => commit(types.RESET_ACTIVE);

export default actions;
