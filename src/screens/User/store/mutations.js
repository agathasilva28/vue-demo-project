import types from './types';

const defaultActive = {};

export const state = {
  active: defaultActive,
};

export const mutations = {
  [types.SET_ACTIVE](state, data) {
    state.active = data;
  },
  [types.UPDATE_ACTIVE](state, { property, value }) {
    state.active = { ...state.active, [property]: value };
  },
  [types.RESET_ACTIVE](state) {
    state.active = defaultActive;
  },
};
