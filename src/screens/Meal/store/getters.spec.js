import getters from './getters';

describe('screens/Meal/store/getters', () => {
  let state;

  beforeEach(() => {
    state = {
      list: [{ id: 1, description: 'Meal' }],
      filters: [{ description: 'Meal' }],
      active: { id: 1, description: 'Meal' },
    };
  });
  
  it('list', () => {
    expect(getters.list(state)).toEqual(state.list);
  });

  it('filters', () => {
    expect(getters.filters(state)).toEqual(state.filters);
  });

  it('active', () => {
    expect(getters.active(state)).toEqual(state.active);
  });
});
