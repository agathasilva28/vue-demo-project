import { sync } from 'vuex-router-sync';
import Vue from 'vue';

import App from './App';
import router from './router';
import store from './store';
import Buefy from 'buefy';

Vue.config.productionTip = false;

Vue.use(Buefy);

sync(store, router);

new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>',
});