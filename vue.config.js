module.exports = {
    runtimeCompiler: true,
    devServer: {
        disableHostCheck: true,
        overlay: {
            warnings: false,
            errors: false
        }
    }
}